import React from 'react';
import PageHeader from './components/PageHeader/PageHeader';
import PageFooter from './components/PageFooter/PageFooter';
import UpdateMessagePopUp from './containers/UpdateMessagePopUp';
import Chat from './containers/Chat';
import './App.css';


function App() {
  return (
    <div className="App">
      <header >
        <PageHeader />
      </header>
      <div className="App-content">
        <Chat />
        <UpdateMessagePopUp />
      </div>
      <footer>
        <PageFooter />
      </footer>
    </div>
  );
}

export default App;
