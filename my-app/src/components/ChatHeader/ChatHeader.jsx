import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { countParticipants, countMessages, updateMessageLastCreated } from '../../containers/Chat/service';
import './styles.css';

export class ChatHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chatName: "My chat"
        }
    }
    
    render() {
        const messages = this.props.messages;
        const participantCount = countParticipants(messages);
        const messagesCount = countMessages(messages);
        const messageLastCreated = updateMessageLastCreated(messages);

        return (
            <div className="header-container">
                <div className="left-part">
                    <div>{this.state.chatName}</div>
                    <div>
                        <span>{participantCount}</span>
                        <span> participants</span>
                    </div>
                    <div>
                        <span>{messagesCount}</span>
                        <span> messages</span>
                    </div>
                </div>
                <div className="right-part">
                    <div>
                        <span>last message at </span>
                        <span>{messageLastCreated}</span>
                    </div>   
                </div>
            </div>
        );
    }
};
ChatHeader.propTypes = {
    messages: PropTypes.array
}
export default ChatHeader;