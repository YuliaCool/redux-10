import React, { Component } from 'react';
import './styles.css';
const LOGO_URL = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/512px-Telegram_logo.svg.png";

export class PageHeader extends Component {

    render() {
        return (
            <div className="header">
                <img src={LOGO_URL} title="logo" alt="logo" />
            </div>
        );
    }
};

export default PageHeader;