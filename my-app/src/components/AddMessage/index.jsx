import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class AddMessage extends Component {

    constructor(props){
        super(props);
        this.state = {
            textBody: undefined
        };
    }

    updateTextArea(ev) {
        return this.setState({ textBody: ev.target.value })
    };

    addMessage = async () => {
        const textBody = this.state.textBody;
        if(!textBody) {
            return;
        }
        this.setState({
            textBody: undefined
        })
        this.props.addMessageHandler(textBody); // TO DO: set empty after sending value
    };
    
    render(){
        return (
            <div className="add-message-container">
                    <textarea className="add-message-input"
                        value={this.state.textBody}
                        placeholder="Message"
                        onChange={ev => this.updateTextArea(ev)}
                    >
                    </textarea>
                <button type="submit" onClick={this.addMessage}> Send</button>
            </div> 
        );
    }
};

AddMessage.propTypes = {
    addMessageHandler: PropTypes.func.isRequired
};

export default AddMessage;