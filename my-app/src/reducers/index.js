import { combineReducers } from 'redux';
import messages from '../containers/Chat/reducer';
import currentUser from '../containers/Chat/CurrentUser/reducer';
import updateMessagePopUp from '../containers/UpdateMessagePopUp/reducer';

const rootReducer = combineReducers({
    messages,
    currentUser,
    updateMessagePopUp
});

export default rootReducer;