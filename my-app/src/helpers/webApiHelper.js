export const get = async (entityName, id) => {
    return await makeRequest(`${entityName}`, 'GET');
}

const makeRequest = async (path, method) => {
    try {
        const url = `${path}`
        const res = await fetch(url, {
            method,
            headers: { "Content-Type": "application/json" }
        });

        return res;
    } catch (err) {
        return err;
    }
}