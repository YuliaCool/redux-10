import { get } from '../helpers/webApiHelper';
const apiPath = "https://api.jsonbin.io/b/5f15f73f9180616628457f77";// https://api.npoint.io/b919cb46edac4c74d0a8

export const getAllMessages = async () => {
    const responce = await get(apiPath);
    return responce.json();
}

export const getCountParticipants = (collectionMessages) => {
    const userIds = new Set();
    collectionMessages.forEach(message => {
        userIds.add(message.userId);
    });
    return userIds.size;
}