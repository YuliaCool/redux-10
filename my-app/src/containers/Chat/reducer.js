import { LOAD_MESSAGE, ADD_NEW_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE } from './actionTypes';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case LOAD_MESSAGE: {
            const { data } = action.payload;
            return [...state, data];
        }
        case ADD_NEW_MESSAGE: {
            const { id, text, currentUser } = action.payload;
            const newMessage = { 
                id,
                text,
                createdAt: new Date(),
                editedAt: "",
                userId: currentUser.id,
                user: currentUser.name,
                avatar: currentUser.avatar,
                countLikes: 0,
                usersLiked: [],
                countDisLikes: 0,
                usersDisLiked: []
            };
            return [...state, newMessage];

        }
        case UPDATE_MESSAGE: {
            const { id, data } = action.payload;
            const updatedMessage = state.map( message => {
                if(message.id === id) {
                    message.text = data;
                    message.editedAt = new Date()
                    return message;
                } else {
                    return message;
                }
            });
            return updatedMessage;
        }
        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const filteredUsers = state.filter(user => user.id !== id);
            return filteredUsers;
        }
        case LIKE_MESSAGE: {
            const { id, userId, key } = action.payload;
            const updatedMessage = state.map( message => {
                if(message.id === id) {
                    if (key === 'like') {
                        if (!message.usersLiked.includes(userId)){
                            message.countLikes += 1;
                            message.usersLiked.push(userId);
                        }
                        else {
                            message.countLikes -= 1;
                            message.usersLiked = message.usersLiked.filter(currentId => currentId !== userId);
                        }

                        if (message.usersDisLiked.includes(userId)){
                            message.countDisLikes -= 1;
                            message.usersDisLiked = message.usersDisLiked.filter(currentId => currentId !== userId);
                        }
                    }
                    if (key === 'dislike') {
                        if (!message.usersDisLiked.includes(userId)){
                            message.countDisLikes += 1;
                            message.usersDisLiked.push(userId);
                        }
                        else {
                            message.countDisLikes -= 1;
                            message.usersDisLiked = message.usersDisLiked.filter(currentId => currentId !== userId);
                        }

                        if (message.usersLiked.includes(userId)){
                            message.countLikes -= 1;
                            message.usersLiked = message.usersLiked.filter(currentId => currentId !== userId);
                        }
                    }
                    return message;
                } else {
                    return message;
                }
            });
            return updatedMessage;
        }
        default:
            return state;
    }
}