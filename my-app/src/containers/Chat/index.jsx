import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import * as service from './service';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import MessageList from '../../components/MessageList/MessageList';
import AddMessage from '../../components/AddMessage';
import { setUpdatingMessageId, showPopUp } from '../UpdateMessagePopUp/actions';

import './styles.css';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
        this.addMessage = this.addMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.editMessage = this.editMessage.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
        this.keyEditMessagePress = this.keyEditMessagePress.bind(this);
    }

    componentDidMount() {
        service.loadMessages()
        .then ( (gottenMessages) => {

            if (gottenMessages) {
                gottenMessages = service.sortMessagesByCreatedAtDate(gottenMessages);
                
                gottenMessages.map( message => {
                    message.countLikes = 0;
                    message.usersLiked = [];
                    message.countDisLikes = 0;
                    message.usersDisLiked = [];

                    message.createdAt = new Date(message.createdAt);
                    if (message.editedAt)
                        message.editedAt = new Date(message.editedAt);
                    store.dispatch(actions.loadMessage(message));
                })
            }
        })
        .then( () => {
            this.setState({ 
                isLoading: false
            });
            document.addEventListener("keydown", this.keyEditMessagePress, false);
        });
    } 
    componentWillUnmount(){
        document.removeEventListener("keydown", this.keyEditMessagePress, false);
    }

    addMessage (text) {
        store.dispatch(actions.addMessage(text, this.props.currentUser));
    }


    deleteMessage(id) {
        store.dispatch(actions.deleteMessage(id));
    }
    editMessage(id) {
        this.props.setUpdatingMessageId(id);
        this.props.showPopUp();
    }

    keyEditMessagePress(e) {
        if(e.code === 'ArrowUp') {
            const lastUserUpdatedMessage = service.findLastMessageByUser(this.props.messages, this.props.currentUser);
            this.editMessage(lastUserUpdatedMessage.id);
        }
    }

    likeMessage(id, likedUserId, key) {
        store.dispatch(actions.likeMessage(id, likedUserId, key));
    }

    render() {
        return (
            <div>
                {   this.state.isLoading 
                    ?  <div className="loader-container">
                            <div className="loader">
                                <span id="one"></span>
                                <span id="two"></span>
                                <span id="three"></span>
                                <span id="four"></span>
                            </div>
                        </div>
                    :  <div className="chat-container">
                        <ChatHeader 
                            messages={this.props.messages}
                        />
                        <MessageList 
                            messages={this.props.messages} 
                            currentUserId={this.props.currentUser.id}
                            onDelete={this.deleteMessage} 
                            onEdit={this.editMessage}
                            onLike={this.likeMessage}
                       />
                       <AddMessage addMessageHandler = { this.addMessage } />
                    </div>
                }
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return { 
        messages: state.messages,
        currentUser: state.currentUser
    }
};

const allActions = {
    ...actions,
    setUpdatingMessageId,
    showPopUp
};

const mapDispatchToProps = dispatch => bindActionCreators(allActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);