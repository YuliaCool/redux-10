import { LOAD_MESSAGE, ADD_NEW_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE } from './actionTypes';
import * as service from './service';

export const loadMessage = data => ({
    type: LOAD_MESSAGE,
    payload: {
        data
    }
});

export const addMessage = (text, currentUser) => ({
    type: ADD_NEW_MESSAGE,
    payload: {
        id: service.getNewId(),
        text,
        currentUser
    }
});

export const updateMessage = (id, data) => ({
    type: UPDATE_MESSAGE,
    payload: {
        id,
        data
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});
export const likeMessage = (id, userId, key) => ({
    type: LIKE_MESSAGE,
    payload: {
        id,
        userId,
        key
    }
});

