import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import { updateMessage } from '../Chat/actions';

import TextInput from '../../components/TextInput';
import './styles.css';

export class UpdateMessagePopUp extends Component {

    constructor(props){
        super(props);

        this.onCancel = this.onCancel.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }
    componentWillReceiveProps(prop){
        if(prop.isShown && prop.messageId) {
            const message = prop.messages.find(message => message.id === prop.messageId);
            this.setState({ text: message.text});
        }
    }

    onChangeData(e) {
        const inputedValue = e.target.value;
        this.setState({ text: inputedValue });
    }

    onCancel = () => {
        this.props.hidePopUp();
        this.setState({ text: '' });
    }
    onEdit = () => {
       this.props.updateMessage(this.props.messageId, this.state.text);
       this.props.hidePopUp();
       this.setState({ text: '' });
    };
    
    renderPopUp(){
        const componentHeader = "Edit message";
        const OkButtonValue = "Ok";
        const CancelButtonValue = "Cancel";

        return (
            <div className="update-message-container">
                <div className="container-header">
                    { componentHeader}
                </div>
               <TextInput text={this.state.text} onChange={this.onChangeData} />
                <div className="button-container">
                    <button type="submit" onClick={this.onEdit}>{ OkButtonValue }</button>
                    <button type="submit" onClick={this.onCancel}>{ CancelButtonValue }</button>
                </div>
            </div> 
        );
    }
    render(){
        return this.props.isShown ? this.renderPopUp() : null;
    }
};

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        isShown: state.updateMessagePopUp.isShown,
        messageId: state.updateMessagePopUp.messageId
    }
}
const mapDispatchToProps = {
    ...actions,
    updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateMessagePopUp);