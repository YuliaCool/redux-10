import { SHOW_POPUP, HIDE_POPUP, SET_UPDATIND_MESSAGE_ID} from './actionsType';

const initialState = {
    messageId: '',
    isShown: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SHOW_POPUP:
            return {
                ...state,
                isShown: true
            };
        case HIDE_POPUP:
            return {
                ...state,
                isShown:false
            };
        case SET_UPDATIND_MESSAGE_ID:
            const { id } = action.payload;
            return {
                ...state,
                messageId: id
            }
        default:
            return state;
    }
}