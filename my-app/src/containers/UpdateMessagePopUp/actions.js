import { SHOW_POPUP, HIDE_POPUP, SET_UPDATIND_MESSAGE_ID } from "./actionsType";

export const showPopUp = () => ({
    type: SHOW_POPUP
});

export const hidePopUp = () => ({
    type: HIDE_POPUP
});

export const setUpdatingMessageId = (id) => ({
    type: SET_UPDATIND_MESSAGE_ID,
    payload: {
        id
    }
});